#include <Arduino.h>
#include <MegunoLink.h>
/* #include <Filter.h> */
/* #include <Plotter.h> */

/* ExponentialFilter<long> ADCFilter(8, 0); */
/* Plotter p; */
TimePlot MyPlot;

int dataA0;
const unsigned threshold = 280;

ISR(TIMER1_OVF_vect)
{
  TCNT1 = 40535; // Timer Preloading
  // Handle The 100ms Timer Interrupt
  //...
  /* digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN)); */
  int raw = analogRead(A0);
  /* ADCFilter.Filter(raw); */
  /* dataA0 = ADCFilter.Current(); */

  if (raw > threshold) {
    digitalWrite(13, HIGH);
    /* delayMicroseconds(interval); */
    MyPlot.SendData(F("Filtered Data (simple low-pass)"), raw);
  }
  else {
    digitalWrite(13, LOW);
    /* delayMicroseconds(interval); */
    /* dataA0 = ADCFilter.Current(); */
  }
}
void setup () {
  Serial.begin(9600);
  pinMode(13, LOW);
  /* ADMUX |= (0 & 0x07);    // set A0 analog input pin */
  pinMode(A0, INPUT);
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);

  MyPlot.SetTitle("My title");
  MyPlot.SetXlabel("samples");
  MyPlot.SetYlabel("filtered signal");
  MyPlot.SetSeriesProperties("ADCValue", Plot::Magenta, Plot::Solid, 2, Plot::Square);
  /* ADCSRA = 0;             // clear ADCSRA register */
  /* ADCSRB = 0;             // clear ADCSRB register */
  /* ADMUX |= (0 & 0x07);    // set A0 analog input pin */

  /* // sampling rate is [ADC clock] / [prescaler] / [conversion clock cycles] */
  /* // for Arduino Uno ADC clock is 16 MHz and a conversion takes 13 clock cycles */
  /* //ADCSRA |= (1 << ADPS2) | (1 << ADPS0);    // 32 prescaler for 38.5 KHz */
  /* ADCSRA |= (1 << ADPS2);                     // 16 prescaler for 76.9 KHz */
  /* ADCSRA |= (1 << ADPS1) | (1 << ADPS0);    // 8 prescaler for 153.8 KHz */
  /* p.Begin(); */
  /* p.AddTimeGraph("Some Title", 500,"Samples", dataA0); */
// initialize timer1
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;

// Set compareMatchReg to the correct value for our interrupt interval
// compareMatchReg = [16, 000, 000Hz / (prescaler * desired interrupt frequency)] - 1

/* E.g. 1Hz with 1024 Pre-Scaler:
   compareMatchReg = [16, 000, 000 / (prescaler * 1)] - 1
   compareMatchReg = [16, 000, 000 / (1024 * 1)] - 1 = 15624

  As this is > 256 Timer 1 Must be used for this..
*/
  const unsigned compareMatchReg = 40535;   // preload timer from calc above
  TCNT1 = compareMatchReg;   // preload timer
/*
  Prescaler:
  (timer speed(Hz)) = (Arduino clock speed(16MHz)) / prescaler
  So 1Hz = 16000000 / 1 --> Prescaler: 1
  Prescaler can equal the below values and needs the relevant bits setting
  1    [CS10]
  8    [CS11]
  64   [CS11 + CS10]
  256  [CS12]
  1024 [CS12 + CS10]
*/
  TCCR1B |= B00000011;    // 64 prescaler

  TIMSK1 |= B00000001;   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
}
void loop () {}
